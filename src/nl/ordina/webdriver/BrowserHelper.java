package nl.ordina.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserHelper {
    public static WebDriver getChromeBrowser() {
        String os = System.getProperty("os.name");
        if("Mac OS X".equals(os)) {
            System.setProperty("webdriver.chrome.driver","lib/chromedriver");
        } else {
            System.setProperty("webdriver.chrome.driver","lib/chromedriver.exe");
        }
        WebDriver driver = new ChromeDriver();
        return driver;
    }
}
